/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.inconsistencycheck;

import java.io.File;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.uidescription.CheckboxPart;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.ListSelectionPart;
import net.sf.okapi.common.uidescription.PathInputPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

    private static final String CHECKINCONSISTENCIES = "checkInconststencies";
    private static final String CHECKPERFILE = "checkPerFile";
    private static final String OUTPUTPATH = "outputPath";
    private static final String DISPLAYOPTION = "displayOption";
    private static final String AUTOOPEN = "autoOpen";
    
    public static final String DISPLAYOPTION_ORIGINAL = "original";
    public static final String DISPLAYOPTION_GENERIC = "generic";
    public static final String DISPLAYOPTION_PLAIN = "plain";
    
    public Parameters() {
        super();
    }

    public boolean getCheckInconststencies () {
    	return getBoolean(CHECKINCONSISTENCIES);
    }
    
    public void setCheckInconststencies (boolean checkInconststencies) {
    	setBoolean(CHECKINCONSISTENCIES, checkInconststencies);
    }
    
    public boolean getCheckPerFile() {
    	return getBoolean(CHECKPERFILE);
    }

    public void setCheckPerFile(boolean checkPerFile) {
    	setBoolean(CHECKPERFILE, checkPerFile);
    }

    public String getOutputPath() {
        return getString(OUTPUTPATH);
    }

    public void setOutputPath(String outputPath) {
        setString(OUTPUTPATH, outputPath);
    }

    public String getDisplayOption () {
        return getString(DISPLAYOPTION);
    }
    
    public void setDisplayOption (String displayOption) {
        setString(DISPLAYOPTION, displayOption);
    }

    public boolean isAutoOpen() {
        return getBoolean(AUTOOPEN);
    }

    public void setAutoOpen(boolean autoOpen) {
        setBoolean(AUTOOPEN, autoOpen);
    }

    @Override
    public void reset() {
		super.reset();
    	setCheckInconststencies(true);
    	setCheckPerFile(false);
    	setOutputPath(Util.ROOT_DIRECTORY_VAR + File.separator + "inconsistency-report.xml");
    	setDisplayOption(DISPLAYOPTION_GENERIC);
    	setAutoOpen(true);
    }

    @Override
    public ParametersDescription getParametersDescription () {
        ParametersDescription desc = new ParametersDescription(this);
        desc.add(CHECKINCONSISTENCIES, "Check inconsistencies", null);
        desc.add(CHECKPERFILE, "Check for inconsistencies on a file-by-file basis", null);
        desc.add(OUTPUTPATH, "Path of the report file:", null);
        desc.add(DISPLAYOPTION, "Representation of the inline codes in the report", null);
        desc.add(AUTOOPEN, "Open the report file after completion", null);
        return desc;
    }

    @Override
    public EditorDescription createEditorDescription (ParametersDescription paramDesc) {
        EditorDescription desc = new EditorDescription("Inconsistency Check", true, false);

        CheckboxPart master = desc.addCheckboxPart(paramDesc.get(CHECKINCONSISTENCIES));
        
        CheckboxPart cbp = desc.addCheckboxPart(paramDesc.get(CHECKPERFILE));
        cbp.setMasterPart(master, true);
        
        PathInputPart pip = desc.addPathInputPart(paramDesc.get(OUTPUTPATH), "Inconsistency Report File", true);
        pip.setMasterPart(master, true);
        
        cbp = desc.addCheckboxPart(paramDesc.get(AUTOOPEN));
        cbp.setMasterPart(master, true);
        
        String[] values = {DISPLAYOPTION_ORIGINAL, DISPLAYOPTION_GENERIC, DISPLAYOPTION_PLAIN};
        String[] labels = {"Original codes", "Generic markers", "Plain text"};
		ListSelectionPart lsp = desc.addListSelectionPart(paramDesc.get(DISPLAYOPTION), values);
        lsp.setChoicesLabels(labels);
        lsp.setListType(ListSelectionPart.LISTTYPE_DROPDOWN);
        lsp.setMasterPart(master, true);
        
        return desc;
    }
}

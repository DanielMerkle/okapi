/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.omegat;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;

public class Options extends StringParameters implements IEditorDescriptionProvider {

	private static final String PLACEHOLDERMODE = "placeholderMode"; //$NON-NLS-1$
	private static final String ALLOWSEGMENTATION = "allowSegmentation"; //$NON-NLS-1$
	private static final String INCLUDEPOSTPROCESSINGHOOK = "includePostProcessingHook"; //$NON-NLS-1$
	private static final String CUSTOMPOSTPROCESSINGHOOK = "customPostProcessingHook"; //$NON-NLS-1$
	
	public Options () {
		super();
	}
	
	@Override
	public void reset() {
		super.reset();
		setPlaceholderMode(true);
		setAllowSegmentation(true);
		setIncludePostProcessingHook(true);
		setCustomPostProcessingHook("");
	}
	
	public boolean getPlaceholderMode () {
		return getBoolean(PLACEHOLDERMODE);
	}

	public void setPlaceholderMode (boolean placeholderMode) {
		setBoolean(PLACEHOLDERMODE, placeholderMode);
	}

	public boolean getAllowSegmentation () {
		return getBoolean(ALLOWSEGMENTATION);
	}
	
	public boolean getIncludePostProcessingHook () {
		return getBoolean(INCLUDEPOSTPROCESSINGHOOK);
	}
	
	public String getCustomPostProcessingHook () {
		return getString(CUSTOMPOSTPROCESSINGHOOK);
	}

	public void setAllowSegmentation (boolean allowSegmentation) {
		setBoolean(ALLOWSEGMENTATION, allowSegmentation);
	}
	
	public void setIncludePostProcessingHook (boolean includePostProcessingHook) {
		setBoolean(INCLUDEPOSTPROCESSINGHOOK, includePostProcessingHook);
	}
	
	public void setCustomPostProcessingHook (String customPostProcessingHook) {
		setString(CUSTOMPOSTPROCESSINGHOOK, customPostProcessingHook);
	}
	
	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(PLACEHOLDERMODE, "Use <g></g> and <x/> notation", null);
		desc.add(ALLOWSEGMENTATION, "Allow segmentation in the OmegaT project",
			"Allow or not segmentation in the project. Ignored if there is a segmentation step.");
		desc.add(INCLUDEPOSTPROCESSINGHOOK, "Include post-processing hook",
			"Set up the project so that OmegaT's \"Create Translated Documents\" command will "
		  + "automatically trigger Okapi's Translation Kit Post-Processing pipeline.");
		desc.add(CUSTOMPOSTPROCESSINGHOOK, "Custom hook", "A custom CLI command to be used as the "
			+ "post-processing hook (leave blank to use default hook).");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("OmegaT Project", true, false);
		desc.addCheckboxPart(paramsDesc.get(PLACEHOLDERMODE));
		desc.addCheckboxPart(paramsDesc.get(ALLOWSEGMENTATION));
		desc.addCheckboxPart(paramsDesc.get(INCLUDEPOSTPROCESSINGHOOK));
		desc.addTextInputPart(paramsDesc.get(CUSTOMPOSTPROCESSINGHOOK)).setAllowEmpty(true);
		return desc;
	}

}

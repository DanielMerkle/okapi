/*===========================================================================
  Copyright (C) 2008-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.steps.xsltransform;

import net.sf.okapi.common.ReferenceParameter;
import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {

	private static final String XSLTPATH = "xsltPath";
	private static final String PARAMLIST = "paramList";
	private static final String USECUSTOMTRANSFORMER = "useCustomTransformer";
	private static final String FACTORYCLASS = "factoryClass";
	private static final String XPATHCLASS = "xpathClass";
	private static final String PASSONOUTPUT = "passOnOutput";

	public Parameters () {
		super();
	}
	
	public void reset () {
		super.reset();
		setXsltPath("");
		setParamList("");
		setUseCustomTransformer(false);
		// Example: net.sf.saxon.TransformerFactoryImpl
		setFactoryClass("");
		setXpathClass("");
		setPassOnOutput(true);
	}
	
	public void setXsltPath (String xsltPath) {
		setString(XSLTPATH, xsltPath);
	}
	
	@ReferenceParameter
	public String getXsltPath () {
		return getString(XSLTPATH);
	}

	public String getParamList() {
		return getString(PARAMLIST);
	}

	public void setParamList(String paramList) {
		setString(PARAMLIST, paramList);
	}

	public boolean getUseCustomTransformer() {
		return getBoolean(USECUSTOMTRANSFORMER);
	}

	public void setUseCustomTransformer(boolean useCustomTransformer) {
		setBoolean(USECUSTOMTRANSFORMER, useCustomTransformer);
	}

	public String getFactoryClass() {
		return getString(FACTORYCLASS);
	}

	public void setFactoryClass(String factoryClass) {
		setString(FACTORYCLASS, factoryClass);
	}

	public String getXpathClass() {
		return getString(XPATHCLASS);
	}

	public void setXpathClass(String xpathClass) {
		setString(XPATHCLASS, xpathClass);
	}

	public boolean getPassOnOutput() {
		return getBoolean(PASSONOUTPUT);
	}

	public void setPassOnOutput(boolean passOnOutput) {
		setBoolean(PASSONOUTPUT, passOnOutput);
	}
}

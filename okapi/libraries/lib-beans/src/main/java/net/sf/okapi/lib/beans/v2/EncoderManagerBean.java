/*===========================================================================
  Copyright (C) 2008-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.beans.v2;

import java.util.Hashtable;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.lib.beans.v1.ParametersBean;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;
import net.sf.okapi.lib.persistence.beans.FactoryBean;

public class EncoderManagerBean extends PersistenceBean<EncoderManager> {
	
	private Hashtable<String, String> mimeMap = new Hashtable<String, String>(); // mimeType to encoder class name map
	private Hashtable<String, FactoryBean> encoders = new Hashtable<String, FactoryBean>(); // mimeType to encoder instance map
	private String mimeType;
	private FactoryBean encoder = new FactoryBean(); // Holds a reference to an encoder in the encoders map	
	private String defEncoding;
	private String defLineBreak;
	private ParametersBean defParams = new ParametersBean();

	@Override
	protected EncoderManager createObject(IPersistenceSession session) {
		return new EncoderManager();
	}

	@Override
	protected void setObject(EncoderManager obj, IPersistenceSession session) {
		for (String mimeType : mimeMap.keySet()) {
			obj.setMapping(mimeType, mimeMap.get(mimeType));
		}
		for (String mimeType : encoders.keySet()) {
			obj.setMapping(mimeType, encoders.get(mimeType).get(IEncoder.class, session));
		}
		if (defLineBreak != null) {
			obj.setDefaultOptions(defParams.get(IParameters.class, session), defEncoding, defLineBreak);
		}		
		obj.updateEncoder(mimeType);
	}

	@Override
	protected void fromObject(EncoderManager obj, IPersistenceSession session) {
		for (String mimeType : obj.getMimeMap().keySet()) {
			mimeMap.put(mimeType, obj.getMimeMap().get(mimeType));
		}
		for (String mimeType : obj.getEncoders().keySet()) {
			FactoryBean encoder = new FactoryBean();
			encoder.set(obj.getEncoders().get(mimeType), session);
			encoders.put(mimeType, encoder);
		}
		encoder.set(obj.getEncoder(), session);
		defEncoding = obj.getEncoding();
		defLineBreak = obj.getDefLineBreak();
		defParams.set(obj.getParameters(), session);
	}

	public Hashtable<String, String> getMimeMap() {
		return mimeMap;
	}

	public void setMimeMap(Hashtable<String, String> mimeMap) {
		this.mimeMap = mimeMap;
	}

	public Hashtable<String, FactoryBean> getEncoders() {
		return encoders;
	}

	public void setEncoders(Hashtable<String, FactoryBean> encoders) {
		this.encoders = encoders;
	}

	public final String getMimeType() {
		return mimeType;
	}

	public final void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
	public FactoryBean getEncoder() {
		return encoder;
	}

	public void setEncoder(FactoryBean encoder) {
		this.encoder = encoder;
	}

	public String getDefEncoding() {
		return defEncoding;
	}

	public void setDefEncoding(String defEncoding) {
		this.defEncoding = defEncoding;
	}

	public String getDefLineBreak() {
		return defLineBreak;
	}

	public void setDefLineBreak(String defLineBreak) {
		this.defLineBreak = defLineBreak;
	}

	public ParametersBean getDefParams() {
		return defParams;
	}

	public void setDefParams(ParametersBean defParams) {
		this.defParams = defParams;
	}

}

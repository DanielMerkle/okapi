package net.sf.okapi.lib.beans.v2;

import net.sf.okapi.filters.idml.NodeReference;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;

public class NodeReferenceBean extends PersistenceBean<NodeReference> {

	private String name;
	private int position;
	
	@Override
	protected NodeReference createObject(IPersistenceSession session) {
		return new NodeReference(name, position);
	}

	@Override
	protected void setObject(NodeReference obj, IPersistenceSession session) {
	}

	@Override
	protected void fromObject(NodeReference obj, IPersistenceSession session) {
		name = obj.name;
		position = obj.position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}

package net.sf.okapi.tkit.integration;

import java.util.LinkedList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.lib.tkit.step.LegacyXliffMergerStep;
import net.sf.okapi.lib.tkit.step.OriginalDocumentXliffMergerStep;
import net.sf.okapi.steps.common.FilterEventsWriterStep;
import net.sf.okapi.steps.common.RawDocumentWriterStep;
import net.sf.okapi.steps.segmentation.Parameters;
import net.sf.okapi.steps.segmentation.SegmentationStep;

public final class RoundTripUtils {
    public static void writeXliff(LocaleId targetLocale, List<Event> events, String root, String path) {
        XLIFFWriter writer = new XLIFFWriter();
        writer.setOptions(targetLocale, "UTF-8");
        writer.setOutput(path);
        
        // Filter events to raw document final step (using the XLIFF writer)
        FilterEventsWriterStep fewStep = new FilterEventsWriterStep();
        fewStep.setDocumentRoots(root);
        fewStep.setFilterWriter(writer);
        fewStep.setOutputURI(Util.toURI(path));
        fewStep.setOutputEncoding("UTF-8");
        fewStep.setLastOutputStep(true);
        fewStep.setTargetLocale(targetLocale);
        
        XLIFFWriterParameters params = (XLIFFWriterParameters)writer.getParameters();
        params.setPlaceholderMode(true);
        params.setIncludeAltTrans(true);
        params.setEscapeGt(true);
        params.setIncludeCodeAttrs(true);
        params.setCopySource(true);
        params.setIncludeIts(true);
        params.setIncludeNoTranslate(true);
        params.setToolId("okapi");
        params.setToolName("okapi-tests");
        params.setToolCompany("okapi");
        params.setToolVersion("M28");
        
        for (Event event : events) {
            fewStep.handleEvent(event);
        }
        writer.close();
        fewStep.destroy();
    }
    
	public static void writeXliff(List<Event> events, String root, String path) {	
	    writeXliff(LocaleId.FRENCH, events, root, path);	    
	}
	
	public static List<Event> segment(List<Event> events) {
		return segment(events, LocaleId.fromString("en-US"), LocaleId.FRENCH);
	}
	
	public static List<Event> segment(List<Event> events, LocaleId source, LocaleId target) {
		List<Event> segmentedEvents = new LinkedList<Event>();
		
		SegmentationStep ss = new SegmentationStep();
		ss.setSourceLocale(source);
		List<LocaleId> tl = new LinkedList<>();
		tl.add(target);
		ss.setTargetLocales(tl);
		Parameters params = (Parameters)ss.getParameters();
		params.setSegmentSource(true);
		params.setSegmentTarget(true);
		params.setSourceSrxPath(RoundTripUtils.class.getClassLoader().getResource("default.srx").getPath());
		params.setTargetSrxPath(RoundTripUtils.class.getClassLoader().getResource("default.srx").getPath());
		//params.setCopySource(false);
		ss.handleEvent(Event.START_BATCH_EVENT);
		ss.handleEvent(Event.START_BATCH_ITEM_EVENT);
		for (Event event : events) {
			segmentedEvents.add(ss.handleEvent(event));
		}
		ss.destroy();
		
		return segmentedEvents;
	}

	public static void tkitMerge(String originalPath, String xlfPath, String outputPath, String filterConfig) {
	      merge(false, originalPath, xlfPath, outputPath, filterConfig, null);
	}
	
    public static void tkitMerge(String originalPath, String xlfPath, String outputPath, String filterConfig, String customConfigPath) {
        merge(false, originalPath, xlfPath, outputPath, filterConfig, customConfigPath);
    }
	
	public static void legacyMerge(String originalPath, String xlfPath, String outputPath, String filterConfig) {
		merge(true, originalPath, xlfPath, outputPath, filterConfig, null);
	}
	
	public static void legacyMerge(String originalPath, String xlfPath, String outputPath, String filterConfig, String customConfigPath) {
		merge(true, originalPath, xlfPath, outputPath, filterConfig, customConfigPath);
	}

    private static void merge(Boolean legacy, String originalPath, String xlfPath, String outputPath, String filterConfig, String customConfigPath) {
    	merge(LocaleId.fromString("en-US"), LocaleId.FRENCH, legacy, originalPath, xlfPath, outputPath, filterConfig, customConfigPath);
    }

	@SuppressWarnings("resource")
    public static void merge(LocaleId source, LocaleId target, Boolean legacy, String originalPath, String xlfPath, String outputPath, String filterConfig, String customConfigPath) {
        FilterConfigurationMapper mapper = new FilterConfigurationMapper();
        DefaultFilters.setMappings(mapper, false, true);
        if (customConfigPath != null) {
            mapper.setCustomConfigurationsDirectory(customConfigPath);
            mapper.addCustomConfiguration(filterConfig);
            mapper.updateCustomConfigurations();
        }
        
        RawDocument originalDoc = new RawDocument(Util.toURI(originalPath), "UTF-8", source, target);
        originalDoc.setFilterConfigId(filterConfig);
        
        IPipelineDriver driver = new PipelineDriver();  
        driver.setFilterConfigurationMapper(mapper);
        BatchItemContext bic = new BatchItemContext(
                new RawDocument(Util.toURI(xlfPath), "UTF-8", source, target), 
                Util.toURI(outputPath), 
                "UTF-8", 
                originalDoc);
        driver.addBatchItem(bic);
        if (legacy) {
            driver.addStep(new LegacyXliffMergerStep());
        } else {
            driver.addStep(new OriginalDocumentXliffMergerStep());
        }
        driver.addStep(new RawDocumentWriterStep());
        driver.processBatch();
        driver.destroy();       
    }
}

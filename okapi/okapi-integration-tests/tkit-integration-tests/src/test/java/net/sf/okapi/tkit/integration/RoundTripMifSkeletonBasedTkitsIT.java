package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.mif.MIFFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripMifSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private MIFFilter mifFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		mifFilter = new MIFFilter();
		testFileList = getFiles();
		URL url = RoundTripMifSkeletonBasedTkitsIT.class.getResource("/mif/Test01.mif");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		mifFilter.close();
	}

	@Test
	public void roundTripMifFiles() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";
			RawDocument rd = new RawDocument(Util.toURI(original),
					"UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(mifFilter, rd, null);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = mifFilter.getConfigurations().get(0).configId;
			RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			FileCompare compare = new FileCompare();
			assertTrue("Original File: " + original, compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
			List<Event> o = FilterTestDriver.getEvents(mifFilter, ord, null);
			List<Event> t = FilterTestDriver.getEvents(mifFilter, trd, null);
			// FIXME: embedded strings with different encodings are not merged correctly
			if (!f.equals("Test01-v7.mif") && !f.equals("Test03_mif7.mif")) {
			    assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, false, true, false, true));
			}
		}
	}

	private static String[] getFiles() throws URISyntaxException {
		URL url = RoundTripMifSkeletonBasedTkitsIT.class.getResource("/mif/Test01.mif");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".mif");
			}
		};
		return dir.list(filter);
	}
}
